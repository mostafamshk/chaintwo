package keeper_test

import (
	"testing"

	testkeeper "chaintwo/testutil/keeper"
	"chaintwo/x/chaintwo/types"
	"github.com/stretchr/testify/require"
)

func TestGetParams(t *testing.T) {
	k, ctx := testkeeper.ChaintwoKeeper(t)
	params := types.DefaultParams()

	k.SetParams(ctx, params)

	require.EqualValues(t, params, k.GetParams(ctx))
}
