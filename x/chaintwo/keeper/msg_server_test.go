package keeper_test

import (
	"context"
	"testing"

	keepertest "chaintwo/testutil/keeper"
	"chaintwo/x/chaintwo/keeper"
	"chaintwo/x/chaintwo/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func setupMsgServer(t testing.TB) (types.MsgServer, context.Context) {
	k, ctx := keepertest.ChaintwoKeeper(t)
	return keeper.NewMsgServerImpl(*k), sdk.WrapSDKContext(ctx)
}
